# -*- coding: utf-8 -*-
"""
Created on Sat Sep 26 14:06:16 2020

@author: DTI
"""
import matplotlib.pyplot as plt
import pickle

class plot:
    
    def __init__(self):
        print("Inicio")
    
    
    def plotarGraficos(self):
        corpos = ["Sol", "Vela - Referência", "Mercúrio", "Vênus", "Terra", "Lua", "Marte", "Júpiter", "Saturno", "Urano", "Netuno"]       
        
        # Lendo arquivos com resultados
        with open('resultadosTempo.txt', 'rb') as filehandle:
            instantesTempo = pickle.load(filehandle)
        with open('resultadosEnergiaSol.txt', 'rb') as filehandle:
            energiaMecanicaEspecifica_VelaSol = pickle.load(filehandle)
        with open('resultadosEnergiaTerra.txt', 'rb') as filehandle:
            energiaMecanicaEspecifica_VelaTerra = pickle.load(filehandle)
        with open('resultadosIntegracao.txt', 'rb') as filehandle:
            y = pickle.load(filehandle)
        with open('resultadosTempoDobro.txt', 'rb') as filehandle:
            instantesTempoDobro = pickle.load(filehandle)
        with open('resultadosEnergiaSolDobro.txt', 'rb') as filehandle:
            energiaMecanicaEspecifica_VelaSolDobro = pickle.load(filehandle)
        with open('resultadosEnergiaTerraDobro.txt', 'rb') as filehandle:
            energiaMecanicaEspecifica_VelaTerraDobro = pickle.load(filehandle)
        with open('resultadosIntegracaoDobro.txt', 'rb') as filehandle:
            yDobro = pickle.load(filehandle)
        with open('resultadosTempoMetade.txt', 'rb') as filehandle:
            instantesTempoMetade = pickle.load(filehandle)
        with open('resultadosEnergiaSolMetade.txt', 'rb') as filehandle:
            energiaMecanicaEspecifica_VelaSolMetade = pickle.load(filehandle)
        with open('resultadosEnergiaTerraMetade.txt', 'rb') as filehandle:
            energiaMecanicaEspecifica_VelaTerraMetade = pickle.load(filehandle)
        with open('resultadosIntegracaoMetade.txt', 'rb') as filehandle:
            yMetade = pickle.load(filehandle)

        
        # Energias
        plt.figure()
        
        plt.subplot(221) # cria plot em 2 dimensoes
        plt.axis('auto')     # define melhor intervalo de valores para eixos
        plt.plot(instantesTempo, energiaMecanicaEspecifica_VelaSol, label = "Número de fios original")
        plt.plot(instantesTempo, energiaMecanicaEspecifica_VelaSolDobro, label = "Dobro do número de fios")
        plt.plot(instantesTempo, energiaMecanicaEspecifica_VelaSolMetade, label = "Metade do número de fios")
        plt.title("Energia específica da E-sail em relação ao Sol", y=1.08, weight='bold')
        plt.xlabel("Tempo [dias]")
        plt.ylabel("Energia específica [J/kg]")
        plt.legend(loc='best')
        plt.grid(True)
        
        plt.subplot(222)
        plt.axis('auto')
        plt.plot(instantesTempo, energiaMecanicaEspecifica_VelaTerra, label = "Número de fios original")
        plt.plot(instantesTempo, energiaMecanicaEspecifica_VelaTerraDobro, label = "Dobro do número de fios")
        plt.plot(instantesTempo, energiaMecanicaEspecifica_VelaTerraMetade, label = "Metade do número de fios")
        plt.title("Energia específica da E-sail em relação à Terra", y=1.08, weight='bold')
        plt.xlabel("Tempo [dias]")
        plt.ylabel("Energia específica [J/kg]")
        plt.legend(loc='best')
        plt.grid(True)
        
        plt.show()
        
        #Trajetorias
        fig = plt.figure()
        # ax = fig.add_subplot(111, projection='3d')  # cria plot em 3 dimensões
        ax = fig.add_subplot(111) # cria plot em 2 dimensoes
        ax.axis('equal')     # iguala os limites e intervalos dos eixos plotados
        
        for i in range(1, len(corpos)):       
            ax.plot(y[:,3*i]-y[:, 0], y[:,3*i+1]-y[:, 1], #y[:,3*i+2]-y[:, 0]  = coordenada Z              
                    '--', label=corpos[i])
        
        ax.plot(yMetade[:,3]-yMetade[:, 0], yMetade[:,4]-yMetade[:, 1], #y[:,3*i+2]-y[:, 0]  = coordenada Z              
                    'k--', label="Vela - Metade do número de fios")
        ax.plot(yDobro[:,3]-yDobro[:, 0], yDobro[:,4]-yDobro[:, 1], #y[:,3*i+2]-y[:, 0]  = coordenada Z              
                    'r--', label="Vela - Dobro do número de fios")
        
        ax.grid(True)
        ax.legend(loc='best')
        plt.show()

        
c = object.__new__(plot)
c.plotarGraficos()