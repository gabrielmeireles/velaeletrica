"""
UNIVERSIDADE FEDERAL DE MINAS GERAIS
ESCOLA DE ENGENHARIA
DEPARTAMENTO DE ENGENHARIA MECANICA
TRABALHO DE CONCLUSÃO DO CURSO DE ENGENHARIA AEROESPACIAL

--- SIMULADOR DE TRAJETÓRIA DE VEÍCULO PROPELIDO POR VELA ELÉTRICA ---
--- AUTOR: RONALDO LUCAS GARCIA CHAVES ---

Observações e simplificações:
- Utiliza referencial inercial
- Origem do referencial está no centro do Sol
- Posições serão usadas em [km], exceto quando especificado
- Velocidades serão usadas em [km/s], exceto quando especificado
- Acelerações serão usadas em [km/s^2], exceto quando especificado
- Contempla a atração gravitacional mútua entre os corpos (Problema de N Corpos) e o modelo dinânimo da vela elétrica
- Código inicialmente inspirado no código do problema de 3 corpos feito por CURTIS, H. D. em seu trabalho "Orbital Mechanics for Engineering Students"
- Este código contém a implementação principal, mas depende das objetos auxiliares 'Body.py' e 'Esail.py'
"""

import numpy as np
from scipy.integrate import RK45
import matplotlib.pyplot as plt
from Base.Body import Body
from Base.Esail import Esail
import math
import sys


class PNC:    
    G = 6.67259e-11   # Constante de gravitacao universal [m^3*s^-1*kg^-2]
    corpos = []
    energiaMecanicaEspecifica_VelaSol = []
    energiaMecanicaEspecifica_VelaTerra = []
    instantesTempo = []
    posicoesCorpos = []
    velocidadesCorpos = []

    # Posições serão usadas em [m]
    # Velocidades serão usadas em [m/s]
    def __init__(self, tempoInicial, tempoFinal):
        self.t0 = tempoInicial          # tempo inicial [s]
        self.tf = tempoFinal            # tempo final [s]

    def main(self):
        self._criarCorpos()    # Cria o corpo principal e todos os demais corpos com suas respectivas posições e velocidades iniciais
        y0 = self._criarVetorUnicoInicial()
        y_total = [y0]         # matriz que receberá informações de cada passo da integracao
        y_total = self._resolveProblemaNCorpos(y0, y_total)
        self._plotaTrajetoria(y_total)
        self._plotaDemaisGraficos()

    # Cria uma lista de objetos do tipo Body.py
    def _criarCorpos(self):
        """
        INSIRA AQUI OS CORPOS QUE SERÃO UTILIZADOS NA SIMULAÇÃO - manter os nomes 'Vela' para sistema veículo + vela elétria, 'Terra' para Terra e 'Sol' para Sol. 
        NÃO REPITA NOMES DE CORPOS! Os nomes serão os identificadores dos corpos ao longo do código.
        O corpo de referência deve ser o primeiro a ser adicionado. 
        """
        self.corpos.append(Body("Sol", 1988500e24, 696000000, 0, 0, 0, 0, 0, 0))
        self.corpos.append(Esail("Vela", 100, 20000, (151043853548.4856 + 42240000)*0.9191914746804555, (151043853548.4856 + 42240000)*-0.393810909672823, (151043853548.4856 + 42240000)*1.7246256262977666e-05, 
                                 (29488.307077734138 + 4500)*0.3810254820256898, (29488.307077734138 + 4500)*0.9245645363546104, (29488.307077734138 + 4500)*-1.2746568298806693e-05, 
                                 100, 20*math.pow(10, -6), 12000, 20000))  # Dados dos fios devem estar no SI
        self.corpos.append(Body("Mercúrio", 3.302e23, 2240000, -5.882263469015850e10, -1.833598259166629e10, 3.897581443175597e9, 4.398195661752813e3, -4.440632006711982E+04, -4.032134010854257E+03))
        self.corpos.append(Body("Vênus", 48.685e23, 6051840, 9.204843389954285E+10, 5.694126226965289E+10, -4.530369353643738E9, -1.853274679295870E+04, 2.963127800863040E+04, 1.476086185148450E+03))
        self.corpos.append(Body("Terra", 5.97219e24, 6371010, 1.388382224846512E+11, -5.948271736641777E+10, 2.604941005244851E+06, 1.123579641841521E+04, 2.726384296120764E+04, -3.758747201825230E-01))
        self.corpos.append(Body("Lua", 7.349e22, 1738000, 1.390287667422883E+11, -5.982122575115648E+10, -1.602867034305632E+07, 1.213344347841932E+03, 2.772624221791948E+04, -7.691848197852913E+01))
        self.corpos.append(Body("Marte", 6.4171e23, 3389920, 2.057801743963098E+11, -2.536448731610999E+10, -5.579598332683106E+09, 3.892862929547761E+03, 2.611718688612092E+04, 4.517968083807151E+02))
        self.corpos.append(Body("Júpiter", 1898.13e24, 69911e3, 3.371437413152705E+11, -6.909170400165222E+11, -4.673401324420542E+09, 1.159680790467136E+04, 6.354783896305077E+03, -2.858004012833475E+02))
        self.corpos.append(Body("Saturno", 5.6834e26, 58232000, 7.387072120843874E+11, -1.301780946511885E+12, -6.768557753899992E+09, 7.876598181236059E+03, 4.749175043238151E+03, -3.962306715695405E+02))
        self.corpos.append(Body("Urano", 86.813e24, 25362000, 2.342064835562079E+12,  1.810364912742804E+12, -2.361488477206206E+10, -4.203840209885813E+03, 5.076882955110641E+03, 7.333338017103763E+01))
        self.corpos.append(Body("Netuno", 102.413e24, 24624000, 4.396937124216954E+12, -8.403803587487594E+11, -8.403819952601546E+10, 9.957243024966468E+02, 5.378833826917628E+03, -1.333341958973633E+02))
    
    # Cria vetor _y0 contendo todas as coordenadas de posições e velocidades iniciais de cada corpo
    def _criarVetorUnicoInicial(self):
        _y0 = []
        for _corpo in self.corpos:
            _posicaoInicialCorpo = _corpo.obterVetorPosicao()
            _y0.append(_posicaoInicialCorpo[0][0])
            _y0.append(_posicaoInicialCorpo[1][0])
            _y0.append(_posicaoInicialCorpo[2][0])

        for _corpo in self.corpos:
            _velocidadeInicialCorpo = _corpo.obterVetorVelocidade()
            _y0.append(_velocidadeInicialCorpo[0][0])
            _y0.append(_velocidadeInicialCorpo[1][0])
            _y0.append(_velocidadeInicialCorpo[2][0])

        return np.array(_y0)

    def _equacionaProblemaNCorpos(self, t, y):
        retorno = []
        _numeroCorpos = len(self.corpos)
        
        "Atualiza velocidade dos corpos:"
        _velocidades = y.copy()     # cria cópia do vetor estado
        _velocidades = _velocidades[::-1]   # coloca os elementos do vetor "de tras pra frente"
        _velocidades = _velocidades[:len(_velocidades)//2]    # apaga as posições da cópia, deixando apenas as velocidades     
        for i in range(_numeroCorpos):  
            # armazena as componentes da velocidade à lista de retorno, tirando elas "de trás pra frente"                           
            vx = _velocidades[-1]
            retorno.append(vx)
            _velocidades = _velocidades[:-1]    # remove ultimo elemento da lista de velocidades, já que ele já foi adicionado ao retorno
            
            vy = _velocidades[-1]
            retorno.append(vy)
            _velocidades = _velocidades[:-1]
            
            vz = _velocidades[-1]
            retorno.append(vz)
            _velocidades = _velocidades[:-1]
            
            # atualiza componentes da velocidade do respectivo corpo (necessário para integrações)
            self.corpos[i].atualizarVelocidade(vx[0], vy[0], vz[0])
            
        _posicoes = y.copy()
        _posicoes = _posicoes[:len(_posicoes)//2]
        _posicoes = _posicoes[::-1]
        for i in range(_numeroCorpos):
            # obtém as posições atualizadas do corpo
            posic_x = _posicoes[-1]
            _posicoes = _posicoes[:-1]
            
            posic_y = _posicoes[-1]
            _posicoes = _posicoes[:-1]
            
            posic_z = _posicoes[-1]
            _posicoes = _posicoes[:-1]
            
            # atualiza componentes da posição do corpo (necessário para integrações)
            self.corpos[i].atualizarPosicao(posic_x[0], posic_y[0], posic_z[0])
        
        _Sol = [body for body in self.corpos if body.obterNome() == "Sol"][0]   
        """
        Cálculo da energia específica
        """
        self._calculaEnergiaMecanicaEspecifica(t, _Sol)
        
        # Armazena posições e velocidades dos corpos para plotar gráficos no final
        # self._armazenaPosicoesEVelocidades(_Sol.obterVetorPosicao(), _Sol.obterVetorVelocidade())
        
        """
        Cálculo das acelerações dos corpos:
        """
        # Adicionando acelerações [km/s^2] na lista de retorno:
        for _corpoRef in self.corpos:
            """Para cada corpo (_corpoRef), calcula-se a influencia gravitacional de cada um dos demais corpos
            (tratados individualmente por _outroCorpo) nele."""
            _demaisCorpos = list([body for body in self.corpos if body.obterNome() != _corpoRef.obterNome()])
            # Cria nova lista com corpos restantes a partir da lista de corpos original, removendo dela o corpo de nome igual ao corpo de referência. 
            # Implica que não existem corpos de mesmo nome na lista.

            _vetorPosicaoCorpoRefBase = _corpoRef.obterVetorPosicao()
            _vetorPosicaoCorpoRef = np.array([[_vetorPosicaoCorpoRefBase[0]], [_vetorPosicaoCorpoRefBase[1]], [_vetorPosicaoCorpoRefBase[2]]])
            
            _vetorAceleracaoCorpoRef = np.array([[[0.]], [[0.]], [[0.]]])
            
            # Verifica se está muito proximo do sol (distancia ao Sol deve ser menor que 0,5 UA)
            if (_corpoRef.obterNome() != "Sol" and _corpoRef.obterNome() != "Mercúrio" and (np.linalg.norm(_vetorPosicaoCorpoRefBase - _Sol.obterVetorPosicao()) < 0.5*149597870691)):
                print("ATENÇÃO! O corpo '{}' se aproximou excessivamente do Sol!".format(_corpoRef.obterNome()))
                sys.exit()
            
            # Calcula influencia dos demais corpos no corpoRef
            for _outroCorpo in _demaisCorpos:
                _vetorPosicaoOutroCorpoBase = _outroCorpo.obterVetorPosicao()
                _vetorPosicaoOutroCorpo = np.array([[_vetorPosicaoOutroCorpoBase[0]], [_vetorPosicaoOutroCorpoBase[1]], [_vetorPosicaoOutroCorpoBase[2]]])
                _massaOutroCorpo = _outroCorpo.obterMassa()

                _vetorPosicaoRelativa = _vetorPosicaoOutroCorpo - _vetorPosicaoCorpoRef
                _moduloPosicaoRelativa = np.linalg.norm(_vetorPosicaoRelativa)
                
                # Verifica colisão
                if (_moduloPosicaoRelativa <= (_corpoRef.obterRaio() + _outroCorpo.obterRaio())):
                    print("ATENÇÃO! Houve colisão entre {} e {}!".format(_corpoRef.obterNome(), _outroCorpo.obterNome()))
                    sys.exit()               

                _vetorAceleracaoCorpoRef += self.G*_massaOutroCorpo*_vetorPosicaoRelativa/math.pow(_moduloPosicaoRelativa,3)
          
            
            """Se o _corpoRef for o sistema veículo + vela elétrica, considera a força propulsiva gerada pela vela."""
            # Dinâmica da vela elétrica - só se aplica quando o corpo ref. for a própria vela
            if isinstance(_corpoRef, Esail):
                r_base = 1.496e+11                                        # Distância de 1 UA [m]
                # v_sw = 400000                                           # Velocidade do vento solar [m/s] (igual a 400 km/s)
                # epsilon_0 = 8.8541878176e-12                            # Permissividade elétrica no vácuo [F/m]
                # e = 1.602176565e-19                                     # Carga elementar [C]
                # m_p = 1.672621777e-27                                   # Massa do próton [kg]
                distanciaSolVela = _corpoRef.obterVetorPosicao() - _Sol.obterVetorPosicao()
                r = np.linalg.norm(distanciaSolVela)                    # Distancia vela-Sol [m]
                # n_base = 7.3e-6                                          # Densidade média de elétrons do vento solar [m^-3]
                # T_e_base = 12*e                                     # Temperatura média do vento solar [K] (igual a 12eV)              
                V = _corpoRef.obterTensao()                             # Tensao da vela [V]
                r_w = _corpoRef.obterRaioFio()                          # Raio do fio constituinte do Hoytether da vela [km]
                N = _corpoRef.obterNumeroFios()                         # Numero de fios da vela
                L = _corpoRef.obterComprimentoFios()                    # Comprimento do fio da vela [m]
                phi = 0                                                 # Angulo de Euler - 1a rotacao - rotação eixo x 
                teta = 0                                                # Angulo de Euler - 2a rotacao - rotação eixo y 
                psi = 0                                                 # Angulo de Euler - 3a rotacao - rotação eixo z 
                
                n1 = 1.1507405980244606e-07 # n1 = 6.18*m_p*(v_sw**2)*math.sqrt(n_base*epsilon_0*T_e_base)/e
                d1 = 1670.349511821751 # d1 = m_p*(v_sw**2)/e
                d2 = 9.531217288370511 # d2 = math.sqrt((epsilon_0*T_e_base)/(n_base*(e**2)))
                
                # Valor base (r = 1 UA) da força propulsiva gerada pela vela por unidade de comprimento
                sigmaF_base = n1 * math.pow((math.expm1((d1/V) * math.log((2/r_w)*d2))), -0.5)
                
                # matriz com ângulos de Euler (phi e teta, aqui nao entra psi) que aparece na fórmula do vetor força propulsiva
                M_angulos = np.array([[math.cos(phi)*math.sin(teta)*math.cos(teta)], 
                                     [-1*math.sin(phi)*math.cos(phi)*(math.cos(teta)**2)], 
                                     [(math.cos(phi)**2)*(math.cos(teta)**2)+1]])                                                                   
                
                # Vetor força propulsiva gerada pela Esail (ref. corpo) 
                vecF_b = 0.5*N*L*sigmaF_base*math.pow(r/r_base, 7/6)*M_angulos
                
                # A_bo = matriz de rotação para passar do ref. corpo para ref. órbita - depende dos angulos de Euler phi, teta e psi
                A_bo = np.array([[math.cos(psi)*math.cos(teta), -1*math.sin(psi)*math.cos(teta), math.sin(teta)],
                                [math.cos(psi)*math.sin(teta)*math.sin(phi)+math.sin(psi)*math.cos(phi), -1*math.sin(psi)*math.sin(teta)*math.sin(phi)+math.cos(psi)*math.cos(phi), -1*math.cos(teta)*math.sin(phi)], 
                                [-1*math.cos(psi)*math.sin(teta)*math.cos(phi)+math.sin(psi)*math.sin(phi), math.sin(psi)*math.sin(teta)*math.cos(phi)+math.cos(psi)*math.sin(phi), math.cos(teta)*math.cos(phi)]])
                
                # Vetor força propulsiva gerada pela Esail (ref. orbita)                                                       
                vecF_o = np.matmul(A_bo, vecF_b)
                
                # RM = matriz de rotação para passar do ref. órbita para ref. inercial
                R_vela_i = _corpoRef.obterVetorPosicao()
                k_i = np.array([0,0,1])     # versor k no ref. inercial
                
                k_o = (1/np.linalg.norm(R_vela_i))*np.array([R_vela_i[0][0], R_vela_i[1][0], R_vela_i[2][0]]) # versor k no ref. orbita, que é igual a posicao da vela no ref. inercial
            
                aux = np.cross(k_i, k_o)
                j_o = (1/np.linalg.norm(aux))*aux  # Versor j no ref. orbita
                
                i_o = np.cross(j_o, k_o) # versor i no ref. orbita
                
                RM =  np.array([i_o, j_o, k_o])
                
                # Vetor força propulsiva gerada pela Esail (ref. inercial)
                vecF_i = np.matmul(RM, vecF_o)
                # print("{:.20f}".format(vecF_i[0][0]))
                
                # Aceleração gerada pela Esail (ref. inercial)
                massa_vela = _corpoRef.obterMassa()
                a_esail_i = (1/massa_vela)*vecF_i
                
                # Adicionando componentes da aceleração da Esail às componentes já calculadas pela interação gravitacional dos corpos
                _vetorAceleracaoCorpoRef += np.array([[a_esail_i[0]], [a_esail_i[1]], [a_esail_i[2]]]) 
            
            
            retorno.append(_vetorAceleracaoCorpoRef[0][0])
            retorno.append(_vetorAceleracaoCorpoRef[1][0])
            retorno.append(_vetorAceleracaoCorpoRef[2][0])
        
        return np.array(retorno)

    def _calculaEnergiaMecanicaEspecifica(self, t_segundos, corpoSol):
        """
        Cálculo da energia mecânica específica da vela em relação à Terra e ao Sol:
        """
        _Terra = [body for body in self.corpos if body.obterNome() == "Terra"][0]
        _Vela = [body for body in self.corpos if body.obterNome() == "Vela"][0] 
        
        _U_VelaTerra = 0.5*(np.linalg.norm(_Vela.obterVetorVelocidade() - _Terra.obterVetorVelocidade())**2) - self.G*_Terra.obterMassa()/np.linalg.norm(_Vela.obterVetorPosicao() - _Terra.obterVetorPosicao())
        _U_VelaSol = 0.5*(np.linalg.norm(_Vela.obterVetorVelocidade() - corpoSol.obterVetorVelocidade())**2) - self.G*corpoSol.obterMassa()/np.linalg.norm(_Vela.obterVetorPosicao() - corpoSol.obterVetorPosicao())
        
        t_dias = t_segundos/(24*3600)
        self.instantesTempo.append(t_dias)
        self.energiaMecanicaEspecifica_VelaTerra.append(_U_VelaTerra)
        self.energiaMecanicaEspecifica_VelaSol.append(_U_VelaSol)
        
    def _armazenaPosicoesEVelocidades(self, posicaoSol, velocidadeSol):        
        if(np.size(self.posicoesCorpos) == 0 and np.size(self.velocidadesCorpos) == 0):
            for i in range(len(self.corpos)):
                self.posicoesCorpos.append([])
                self.velocidadesCorpos.append([])
                
        for k in range(len(self.corpos)):
            if(self.corpos[k].obterNome() != "Sol"):
                self.posicoesCorpos[k].append(np.linalg.norm(self.corpos[k].obterVetorPosicao() - posicaoSol))
                self.velocidadesCorpos[k].append(np.linalg.norm(self.corpos[k].obterVetorVelocidade() - velocidadeSol))           
    
    def _resolveProblemaNCorpos(self, vetorUnicoInicial, y_total):    
        # integrador runge kutta de 4(5) ordem, onde 86400/2 representa o passo máximo:
        _rungeKutta = RK45(self._equacionaProblemaNCorpos, self.t0, vetorUnicoInicial, self.tf,
                          86400/2, 1e-3, 1e-6, True)

        while _rungeKutta.status == "running":
            # passo da integracao:
            _rungeKutta.step()
            # armazena resultado do passo no vetor y_total:
            y_total = np.append(y_total, [_rungeKutta.y], axis=0)
            # imprime instante de tempo (em dias) em que foi realizado o passo da integração:
            t_dias = _rungeKutta.t/(24*3600)
            print(t_dias)
            # Armazena a energia específica da vela e o instante de tempo deste passo
            # self.instantesTempo.append(t_dias)
            # self._calculaEnergiaMecanicaEspecifica()
            
        return y_total      
        
    def _plotaTrajetoria(self, y):
        fig = plt.figure()
        # ax = fig.add_subplot(111, projection='3d')  # cria plot em 3 dimensões
        ax = fig.add_subplot(111) # cria plot em 2 dimensoes
        ax.axis('equal')     # iguala os limites e intervalos dos eixos plotados
        
        for i in range(1, len(self.corpos)):       
            ax.plot(y[:,3*i]-y[:, 0], y[:,3*i+1]-y[:, 1], #y[:,3*i+2]-y[:, 0]  = coordenada Z              
                    '--', label=self.corpos[i].obterNome())

        ax.grid(True)
        ax.legend(loc='best')
        plt.show()
    
    def _plotaDemaisGraficos(self):
        # Grafico de energia especifica x tempo
        plt.figure()
        
        plt.subplot(221) # cria plot em 2 dimensoes
        plt.axis('auto')     # define melhor intervalo de valores para eixos
        plt.plot(self.instantesTempo, self.energiaMecanicaEspecifica_VelaSol)
        plt.title("Energia específica da E-sail em relação ao Sol", y=1.08, weight='bold')
        plt.xlabel("Tempo [dias]")
        plt.ylabel("Energia específica [J/kg]")
        plt.grid(True)
        
        plt.subplot(222)
        plt.axis('auto')
        plt.plot(self.instantesTempo, self.energiaMecanicaEspecifica_VelaTerra)
        plt.title("Energia específica da E-sail em relação à Terra", y=1.08, weight='bold')
        plt.xlabel("Tempo [dias]")
        plt.ylabel("Energia específica [J/kg]")
        plt.grid(True)
        
        plt.show()
        
        # Graficos de posicao x tempo e velocidade x tempo
        # plt.figure()
        
        # plt.subplot(223) # cria plot em 2 dimensoes
        # plt.axis('auto')     # define melhor intervalo de valores para eixos
        # for i in range(len(self.corpos)):
        #     if(self.corpos[i].obterNome() != "Sol"):
        #         plt.plot(self.instantesTempo, self.posicoesCorpos[i], label=self.corpos[i].obterNome())

        # plt.title("Posição dos corpos em relação ao Sol", y=1.08, weight='bold')
        # plt.xlabel("Tempo [dias]")
        # plt.ylabel("Posição em relação ao Sol [m]")
        # plt.grid(True)
        # plt.legend(loc='best')
        
        # plt.subplot(224)
        # plt.axis('auto')
        # for i in range(len(self.corpos)):
        #     if(self.corpos[i].obterNome() != "Sol"):
        #         plt.plot(self.instantesTempo, self.velocidadesCorpos[i], label=self.corpos[i].obterNome())
        
        # plt.title("Velocidade dos corpos em relação ao Sol", y=1.08, weight='bold')
        # plt.xlabel("Tempo [dias]")
        # plt.ylabel("Velocidade em relação ao Sol [m/s]")
        # plt.grid(True)       
        # plt.legend(loc='best')
        
        # plt.show()

    
"""
INSIRA AQUI OS INSTANTES DE TEMPO INICIAL E FINAL DA SIMULAÇÃO [segundos]
"""
p1 = PNC(0.0, 365*24*3600)
p1.main()