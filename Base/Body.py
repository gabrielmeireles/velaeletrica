import numpy as np


# Objeto genérico representando qualquer corpo que será usado na simulação
class Body:

    # Ao inicializar, recebe a massa e a posição inicial em cada um dos três eixos
    # Adiciona posições à um array
    def __init__(self, nomeCorpo, massaCorpo, raioCorpo, posicaoInicialEixoX, posicaoInicialEixoY, posicaoInicialEixoZ,
                 velocidadeInicialEixoX, velocidadeInicialEixoY, velocidadeInicialEixoZ):
        self.nome = nomeCorpo
        self.massa = massaCorpo
        self.raio = raioCorpo
        self.posicao_x = posicaoInicialEixoX
        self.posicao_y = posicaoInicialEixoY
        self.posicao_z = posicaoInicialEixoZ
        self.veloc_x = velocidadeInicialEixoX
        self.veloc_y = velocidadeInicialEixoY
        self.veloc_z = velocidadeInicialEixoZ

    # Retorna massa do corpo
    def obterMassa(self):
        return self.massa

    def obterRaio(self):
        return self.raio

    # Retorna vetor posição inicial do corpo
    def obterVetorPosicao(self):
        return np.array([[self.posicao_x], [self.posicao_y], [self.posicao_z]])

    # Retorna vetor velocidade inicial do corpo
    def obterVetorVelocidade(self):
        return np.array([[self.veloc_x], [self.veloc_y], [self.veloc_z]])
    
    def obterNome(self):
        return self.nome
    
    def atualizarPosicao(self, x, y, z):
        self.posicao_x = x
        self.posicao_y = y
        self.posicao_z = z
        
    def atualizarVelocidade(self, vx, vy, vz):
        self.veloc_x = vx
        self.veloc_y = vy
        self.veloc_z = vz
        
