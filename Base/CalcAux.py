import math


class CalculosAuxliares:   
    r_base = 149597870691                                   # Distância de 1 UA [m]
    v_sw = 400000                                           # Velocidade do vento solar [m/s] (igual a 400 km/s)
    epsilon_0 = 8.854187817600000e-12                       # Permissividade elétrica no vácuo [F/m]
    e = 1.602176565e-19                                     # Carga elementar [C]
    m_p = 1.672621777e-27                                   # Massa do próton [kg]
    # distanciaSolVela = _corpoRef.obterVetorPosicao() - _posicaoSol
    # r = np.linalg.norm(distanciaSolVela)                  # Distancia vela-Sol [m]
    n_base = 7.3e6                                          # Densidade média de elétrons do vento solar [m^-3]
    T_e_base = 12*e                                         # Temperatura média do vento solar [J] (igual a 12eV)              
    # V = _corpoRef.obterTensao()                           # Tensao da vela [V]
    #r_w = _corpoRef.obterRaioFio()                         # Raio do fio constituinte do Hoytether da vela [km]
    #N = _corpoRef.obterNumeroFios()                        # Numero de fios da vela
    #L = _corpoRef.obterComprimentoFios()                   # Comprimento do fio da vela [m]
    phi = 0                                                 # Angulo de Euler - 1a rotacao - rotação eixo x 
    teta = 0                                                # Angulo de Euler - 2a rotacao - rotação eixo y 
    psi = 0                                                 # Angulo de Euler - 3a rotacao - rotação eixo z 
    
    numerador =  6.18*m_p*(v_sw**2)*math.sqrt(n_base*epsilon_0*T_e_base)
    numerador_sobre_e = numerador/e #n1 no simulador
    den2 = math.sqrt((epsilon_0*T_e_base)/(n_base*(e**2))) #d1 no simulador
    den1 = m_p*(v_sw**2)/e #d2 no simulador
    # Valor base (r = 1 UA) da força propulsiva gerada pela vela por unidade de comprimento
    # sigmaF_base = numerador_sobre_e*(1/(math.sqrt(math.expm1((1/V)*den1*math.log((2/r_w)*den2)))))
    print("n1")
    print(numerador_sobre_e)
    print("\n\n")
    print("d1")
    print(den1)
    print("\n\n")
    print("d2")
    print(den2)
    
    # dados iniciais retirados da HORIZONS para dia 30/09/2020 00:00:00 TDB
    X = 1.388382224846512E+11 
    Y =-5.948271736641777E+10
    Z = 2.604941005244851E+06
    normPos = math.sqrt(X**2 + Y**2 + Z**2)
    print("\n\n")
    print("posição absoluta terra")
    print(normPos)
    print("\n\n")
    print("vetor posicao unitário")
    print(X/normPos)
    print(Y/normPos)
    print(Z/normPos)
    
    VX= 1.123579641841521E+04 
    VY= 2.726384296120764E+04 
    VZ=-3.758747201825230E-01
    normVel = math.sqrt(VX**2 + VY**2 + VZ**2)
    print("\n\n")
    print("velocidade absoluta terra")
    print(normVel)
    print("\n\n")
    print("vetor velocidade unitário")
    print(VX/normVel)
    print(VY/normVel)
    print(VZ/normVel)