# E-sail com configuração de malha ou configuração circular com N fios (N deve ser múltipo de 4)
from Base.Body import Body


class Esail(Body):
    
    def __init__(self, nomeCorpo, massaCorpo, raioCorpo, posicaoInicialEixoX, posicaoInicialEixoY, posicaoInicialEixoZ,
                 velocidadeInicialEixoX, velocidadeInicialEixoY, velocidadeInicialEixoZ,
                 numeroFios, raioFio, tensaoFio, comprimentoFios):
        self.N = numeroFios
        self.r_w = raioFio
        self.V = tensaoFio
        self.L = comprimentoFios
        super().__init__(nomeCorpo, massaCorpo, raioCorpo, posicaoInicialEixoX, posicaoInicialEixoY, posicaoInicialEixoZ,
                 velocidadeInicialEixoX, velocidadeInicialEixoY, velocidadeInicialEixoZ)
        
    
    def obterNumeroFios(self):
        return self.N
    
    
    def obterRaioFio(self):
        return self.r_w
    
    
    def obterTensao(self):
        return self.V
    
    def obterComprimentoFios(self):
        return self.L